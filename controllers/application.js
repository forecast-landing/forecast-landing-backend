const applicationService = require('../services/application');
const lg = console.log;

class ApplicationController {
    async createApplication (req, res) {
        const { body } = req;
        try {
            const result = await applicationService.createApplication(body);
            res.status(201).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json(error);
        }
    }
    async getAllApplications (req, res) {
        try {
            const result = await applicationService.getAllApplications();
            res.status(200).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json(error);
        }
    }

    async deleteApplication(req, res) {
        const {id} = req.params;
        try {
            const result = await applicationService.deleteApplication(id);
            res.status(200).json(result);
        } catch (error) {
            lg(error);
            const statusCode = error.statusCode || 500;
            res.status(statusCode).json(error);
        }
    }
}

const applicationController = new ApplicationController();
module.exports = applicationController;