const mongoose = require('mongoose');

const applicationSchema = new mongoose.Schema({
    name : { type: String, require: true },
    companyName : { type: String, require: true },
    position : { type: String, require: true },
    email : { type: String, require: true },
    phoneNumber : { type: String, require: true },
    stationDescription : { type: String, require: true },
    comment : { type: String, require: true }
})

const Application = new mongoose.model('Application', applicationSchema);

module.exports = Application;