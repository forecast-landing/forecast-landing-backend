const express = require('express')
const applicationController = require('../controllers/application');

const router = express.Router();

router.post('/applications', applicationController.createApplication);
router.get('/applications', applicationController.getAllApplications);
router.delete('/applications/:id', applicationController.deleteApplication);

module.exports = router;



/**
 * @swagger
 * components:
 *   schemas:
 *     NewApplication:
 *       type: object
 *       required:
 *         - name
 *         - companyName
 *         - position
 *         - email
 *         - phoneNumber
 *         - stationDescription
 *         - comment
 *       properties:
 *         name:
 *           type: string
 *         companyName:
 *           type: string
 *         position:
 *           type: string
 *         email:
 *           type: string
 *         phoneNumber:
 *           type: string
 *         stationDescription:
 *           type: string
 *         comment:
 *           type: string
 *
 *     ErrorObject:
 *       type: object
 *       properties:
 *         statusCode:
 *           type: integer
 *         errors:
 *           type: object
 *           properties:
 *             name:
 *               type: string
 *             companyName:
 *               type: string
 *             position:
 *               type: string
 *             email:
 *               type: string
 *             phoneNumber:
 *               type: string
 *             stationDescription:
 *               type: string
 *             comment:
 *               type: string
 */

/**
 * @swagger
 * /applications:
 *   post:
 *     summary: Create a new application
 *     tags:
 *       - Applications
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/NewApplication'
 *     responses:
 *       201:
 *         description: Application created successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/NewApplication'
 *       400:
 *         description: Invalid request body or missing required fields
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ErrorObject'
 *         example:
 *           statusCode: 400
 *           errors:
 *             name: name is not provided
 *             companyName: companyName is not provided
 *             position: position is not provided
 *             email: email is not provided
 *             phoneNumber: phoneNumber is not provided
 *             stationDescription: stationDescritpion is not provided
 *             comment: comment is not provided
 *       500:
 *         description: Internal server error
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     Application:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         name:
 *           type: string
 *         companyName:
 *           type: string
 *         position:
 *           type: string
 *         email:
 *           type: string
 *         phoneNumber:
 *           type: string
 *         stationDescription:
 *           type: string
 *         comment:
 *           type: string
 *
 *     ApplicationsResponse:
 *       type: array
 *       items:
 *         $ref: '#/components/schemas/Application'
 */

/**
 * @swagger
 * /applications:
 *   get:
 *     summary: Retrieve all applications
 *     tags:
 *       - Applications
 *     responses:
 *       200:
 *         description: List of applications
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/ApplicationsResponse'
 *         example:
 *           - id: "6681928a43489858f7ff8e3e"
 *             name: "Alikhan"
 *             companyName: "tgs"
 *             position: "backend developer"
 *             email: "alikhankaliyev20@gmail.com"
 *             phoneNumber: "+77772902908"
 *             stationDescription: "ses"
 *             comment: "cdadadada"
 *           - id: "6681928c43489858f7ff8e40"
 *             name: "Alikhan"
 *             companyName: "tgs"
 *             position: "backend developer"
 *             email: "alikhankaliyev20@gmail.com"
 *             phoneNumber: "+77772902908"
 *             stationDescription: "ses"
 *             comment: "cdadadada"
 *       500:
 *         description: Internal server error
 */

/**
 * @swagger
 * components:
 *   schemas:
 *     DeleteResponse:
 *       type: object
 *       properties:
 *         message:
 *           type: string

/**
 * @swagger
 * /applications/{id}:
 *   delete:
 *     summary: Delete an application by ID
 *     tags:
 *       - Applications
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *         description: ID of the application to delete
 *     responses:
 *       200:
 *         description: Application deleted successfully
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/DeleteResponse'
 *         example:
 *           message: Application with id:6681928a43489858f7ff8e3e is deleted
 *       404:
 *         description: Application with such an id is not found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *         example:
 *           message: Application with such an id is not found
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 message:
 *                   type: string
 *         example:
 *           message: Internal server error
 */