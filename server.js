require('dotenv').config();
const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');
const routes = require('./routes');
const swaggerUI = require('swagger-ui-express');
const swaggerSpec = require('./config/swagger');

const app = express();

app.use(cors({ origin: true }));

app.use(bodyParser.json());

app.use(routes);

app.use('/api-docs', swaggerUI.serve, swaggerUI.setup(swaggerSpec));

mongoose.connect(process.env.mongodb_url, {
    useNewUrlParser: true,
    useUnifiedTopology: true
});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


const port = process.env.PORT;


app.listen(port, () => {
    console.log(`Server is running on port ${port}`)
});
