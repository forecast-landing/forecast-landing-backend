const Application = require('../models/application');
const CustomError = require('../errors/custom_error');
const validator = require('validator');
const { application } = require('express');

class ApplicationService {
    async createApplication(body) {
        if (body == null) throw new CustomError('no data provided', 400, null);
        const {name, companyName, position, email, 
            phoneNumber, stationDescription, comment} = body;
        const errors = {};
        if (!name) errors.name = 'name is not provided';
        if (!companyName) errors.companyName = 'companyName is not provided';
        if (!position) errors.position = 'position is not provided';
        if (!email) errors.email = 'email is not provided';
        if (!phoneNumber) errors.phoneNumber = 'phoneNumber is not provided';
        if (!stationDescription) errors.stationDescription = 'stationDescritpion is not provided';
        if (!comment) errors.comment = 'comment is not provided';
        if (email && !validator.isEmail(email)) errors.email = 'emails is not valid';
        if (phoneNumber && !validator.isMobilePhone(phoneNumber)) errors.phoneNumber = 'phoneNumber is not valid';
        if (Object.keys(errors).length > 0) {
            throw new CustomError('Validation failed', 400, errors);
        }
        const application = new Application({
            name,
            companyName,
            position,
            email,
            phoneNumber,
            stationDescription,
            comment
        });

        await application.save();

        return application;
    }

    async getAllApplications() {
        const applications = await Application.find().lean();
            return applications.map(application => {
                const { _id, __v, ...rest } = application;
                return {
                    id: _id,
                    ...rest
                };
            });
    }

    async deleteApplication(id) {
        if (!await Application.findByIdAndDelete(id)) {
            throw new CustomError('Application with such an id is not found', 404);
        }
        return {
            message: `Application with id:${id} is deleted`
        };
    }
}

const applicationService = new ApplicationService();

module.exports = applicationService;